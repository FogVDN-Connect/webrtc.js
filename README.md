# webrtc.js

#### 描述
- 最简单的datechannel, 其中datachannel的js主要是针对pear_webrtc（base-webrtc）来编写。
- pear_webrtc为在终端运行base-webrtc模块，其具备在Linux(e.g. Ubuntu, OpenWrt), Windows, Android, iOS等平台上运行。
- 参照其信令协议，可开发更强大的功能（其多源多协议，请参考：[PearPlayer](https://gitee.com/PearInc/PearPlayer.js)）。
- pear_webrtc对资源消耗极少（一般运行状态下内存占用5-10M，峰值不超过30M）
- 可随业务，修改其信令协议（在作者的参与下）。




#### 运行
- 首先在终端上启动pear_webrtc。如：./pear_webrtc (在newifi上等)。
- 在其pear_webrtc的工作目录下（运行目录），放上所需要的测试的文件。如：(./image/pear.png).
- 使用支持webrtc本版的浏览器，打开index.html.
- 在输入框里输入在终端被测试文件的路径（相对于pear_webrtc的工作目录）。如：（image/pear.png).
- 然后点击按钮即可。

#### 运行示图
![demo](./image/demo.png)

### 支持
- 作者：wulei Pear Limited
- e-mail: w@pear.hk.