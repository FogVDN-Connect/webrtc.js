###  wulei Pear Limited
###  peer_id random by browser
###  peer_id is mac address by node 

# file/data的传输命令
##### 块请求
- pear_webrtc接收文件请求信心，目前browser是以1M 为chunk大小发送请求（可以根据需要修改）
- 请注意start,end其获取的数据大小为(end-start)+1.
```html
6. browser --> node 
{
     "host:"xxx.com", 
     "uri":"image/pear.png", 
     "action":"get",
     "response_type":"binary",  //也可选base64，默认为二进制。
     "start":0,                 //文件开始的地址
     "end":5242879              //文件截止的地址(包括此地址，如buf[5242879])
}
```

##### 块请求返回
```html
node --> browser
6(1). 
{
   "begin":true,
   "host:"xxx.com", 
   "uri":"image/pear.png", 
   "chunks":1024		//告诉接受端，本次块请求的数据大小(end-start)+1，datachannel通道需要传输多少次。
}
```
##### 块请求的数据返回
6(2). 
- base64模式
```html
node --> browser
{
   "host:"xxx.com", 
   "uri":"image/pear.png", 
   "value":"data:;base64,iVBORw…",    # 目前不是直接二进制传输，而是base64编码
   "start":0,
   "end":4096      	# 此消息不宜过大，目前pear_webrtc是配置为32K(最大64K,还需要考虑base64编码)
}
Base64编码会把3字节的二进制数据编码为4字节的文本数据，长度增加33%,为了提升效率采用二进制传输
```
- binary模式
```html
node --> browser
0--255字节(不够的部分0X00补齐)：
{
   "host:"xxx.com",
   "uri":"image/pear.png",
   "value":"",     //为空即可
   "start" : 0,
   "end":1024
}
256--结束：
value:payload
```

##### 完成块请求
```html
6(3).
node --> browser
{
   "done":true,
   "host:"xxx.com", 
   "uri":"image/pear.png"
}
```

##### 块请求的节点没这个缓存文件
- 节点没有缓存的话，直接返回错误码，browser重新向signal server申请其它节点信息。
```html
6(4).
node --> browser
{
   "errorcode":4004,
   "msg":"No such file!"
}
```






