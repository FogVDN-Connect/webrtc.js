###  wulei Pear Limited
###  peer_id random by browser
###  peer_id is mac address by node 
###  目前的模式node是作为offer的发出方。


# 资源获取命令与返回信息
##### browser从信令服务器获取pear.mp4资源的节点get命令
```html
1.  browser ---> signal server
{
  "action":"get",
  "peer_id":"1555465897-OkVO2mSjj",		//random by browser (不大于32byte)。
  "host":"xxx.com",				//CP域名。	
  "uri":"video/pear.mp4",			//资源在节点上的路径。
  "md5":"..."					md5("xxx.com"+"video/pear.mp4").
}
```

##### get命令有节点的信息
2. signaling server ------> browser
```html
{
  "nodes": [
        {
          "peer_id":"mac1",				//peer_id is mac address by node 	
           "sdp":{"type":"offer","sdp":"xxxx"},		//其中"sdp":"xxxx"为node的offer信息。
           "offer_id":1122334455667788,
           "host":"xxx.com",
           "uri":"video/pear.mp4",
           "md5":"...",
        },

	{
	   "peer_id":"mac2",
	   "sdp":{"type":"offer","sdp":"xxxx"},
	   "offer_id":1122334455667799,
	   "host":"xxx.com",
	   "uri":"video/pear.mp4",
	   "md5":"..."
	},
	..........
    ]
}
```
##### get命令返回返回无节点的信息
2(1). signaling server ------> browser
```html
{
   "errorcode":"4004",
   "msg":"No such file",
   "host":"xxx.com",
   "uri":"video/pear.mp4",
   "md5":"..."
}
```

# datachannel创建
- 因node做offer方，所以browser在接受到offer后，生成answer转发给node.
##### answer转发
```html
3. browser ---> signal server  --->  node
{
  "action":"answer",
  "peer_id":"1555465897-OkVO2mSjj",			//注意这里，为get命令时的peer_id字段信息。
  "to_peer_id":"mac",					//注意这里，为返回的nodes里的mac （查看2）。
  "offer_id":1122334455667788,				//注意这里，为返回的nodes里的offer_id （查看2）
  "sdps":{"type":"answer","sdp":"xxxx"}			//具体查看dc.js
}
```

##### candidate转发
```html
4. browser ---> signal server  --->  node 
{
  "action":"candidate",
  "peer_id":"1555465897-OkVO2mSjj",
  "to_peer_id":"mac",
  "offer_id":1122334455667788,
  "candidates":
   { "candidate": "","sdpMid": "data","sdpMLineIndex": 0 }	//具体查看dc.js
}
```

##### completed转发
- 这命令是browser告诉node已经完成全部candidate的转发。
- 这命令在peerConnection.onicegatheringstatechange中发送。
```html
5. browser ---> signal server  --->  node
{
    "peer_id":"1555465897-OkVO2mSjj",
    "to_peer_id":"mac",
     "offer_id":1122334455667788,
     "action":"candidate",
     "candidates":{
		"candidate":"completed"
      }
}
```

### browser发送心跳信息，保持与pear_webrtc的连接
- 在建立起datachannel通道后，node的应用层需要继续“保活”，默认是120s内没有任何数据传输就会自动关闭datachannel通道。
- 建议90s发送一次心跳信息。
- 注意这命令是通过datachannel通道发送的。
```html
6. browser --> node
{
     "action":"ping"
}
```








